﻿''' ###############################################################################################################
''' #                                               INFORMATIONS                                                  #
''' ###############################################################################################################
''' #                                                                                                             #
''' # Nom du fichier         :  Connexion.vb                                                                      #                                                             
''' # Rôle du fichier        :  Interface de connexion                                                            #
''' # Langage                :  Visual Basic                                                                      #
''' # Auteur                 :  Kilian Magniez                                                                    #
''' # Dernière modification  :  14/12/2018                                                                        #
''' # Description            :  envois des information du pompier dans un autre form                              #
''' #                                                                                                             #
''' ###############################################################################################################
''' #                                                 VARIABLES                                                   #
''' ###############################################################################################################
''' #                                                                                                             #
''' # FGD           : Objet                                                                                       #
''' # Result        : Objet                                                                                       #                                                                                                                                                   #
''' # Nom_Pompier   : String                                                                                      #
''' # PrenomPompier : String                                                                                      #
''' # BIP_Pompier   : String                                                                                      #
''' #                                                                                                             #
''' ###############################################################################################################


Public Class ecranConnexion
    ' adamal
    ' CBED523D009489465B9EA1EE3605B48B
    Dim FDG As FeuilleDeGarde
    Public listeUtilisateur As New ArrayList
    Dim Result As DataRow
    Dim requeteConnexion As String
    Dim requeteInfoPompier As String

    Public Shared instance As Connexion

    Private Sub Connexion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Affichage du nom de la machine local avec la fonction GetHostname
        TB_Statut.Text = "Nom du PC : " & GetHostname()

        'Affichage de l'adresse IP de la machine local avec la fonction GetIP()
        TB_Statut.Text = TB_Statut.Text & vbCrLf & "IP du PC : " & GetIP()


        TB_Statut.Text = TB_Statut.Text & vbCrLf & ""
        TB_Statut.Text = TB_Statut.Text & vbCrLf & "Connexion à la base SDIS29..."
        instance = New Connexion("odbcExamen", "SDIS29", "Iroise29")

        'Masquage des caractères écrit dans la TextBox TB_Motdepasse. Les caractère sont remplacés par des étoiles ("*")
        TB_Motdepasse.PasswordChar = "*"




    End Sub



    Private Sub BT_Connexion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BT_Connexion.Click
        Dim cnxUtilisateur As New utilisateur
        requeteConnexion = "SELECT LOG_LOGIN, LOG_MDP FROM LOGIN WHERE LOG_LOGIN='" & TB_Identifiant.Text & "' AND LOG_MDP='" & TB_Motdepasse.Text & "';"
        requeteInfoPompier = "SELECT LOGIN.SP_MATRICULE, LOG_NOM,LOG_PRENOM, SP_BIP FROM LOGIN, POMPIER WHERE LOG_LOGIN='" & TB_Identifiant.Text & "' AND LOGIN.SP_MATRICULE=POMPIER.SP_MATRICULE;"

        Try
            Connexion.ORA.Execute(requeteConnexion)


            Result = Connexion.ORA.Champ(requeteInfoPompier)
            MsgBox("connexion réussi")
            cnxUtilisateur.fetchPompier(Result("SP_MATRICULE"))
            'listeUtilisateur.Add(unUtilisateur)
            FDG = New FeuilleDeGarde(cnxUtilisateur)
            FDG.Visible = True


        Catch ex As Exception
            LB_message.Text = "/!\ Problème de Connexion /!\"


        End Try



    End Sub
End Class
