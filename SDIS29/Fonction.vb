﻿''' ###############################################################################################################
''' #                                               INFORMATIONS                                                  #
''' ###############################################################################################################
''' #                                                                                                             #
''' # Nom du fichier         :  Fonction.vb                                                                       #                                                             
''' # Rôle du fichier        :  Contient toute les fonctions                                                      #
''' # Langage                :  Visual Basic                                                                      #
''' # Auteur                 :  Kilian Magniez                                                                    #
''' # Dernière modification  :  23/11/2018                                                                        #
''' # Description            :  Ajout de commentaires                                                             #
''' #                                                                                                             #
''' ###############################################################################################################
''' #                                                 FONCTIONS                                                   #
''' ###############################################################################################################
''' #                                                                                                             #                                                             
''' # GetHostname()  : Retourne le nom de la machine locale                                                       #
''' # GetTP()        : Retourne l'adresse IP de la machine Locale                                                 #
''' #                                                                                                             #
''' ###############################################################################################################
'''  #                                                VARIABLES                                                   #
''' ###############################################################################################################
''' #                                                                                                             #
''' # Hostname   : String                                                                                         #                                                             
''' # Hostname2  : String                                                                                         #
''' # ip         : Objet                                                                                          #
''' #                                                                                                             #
''' ###############################################################################################################



Module Fonction


    Public Function GetHostname()
        'Retourne le nom de la machine locale

        Dim hostname As String

        hostname = Environment.MachineName()

        Return hostname

    End Function



    Public Function GetIP()

        'retourne l'adresse IP locale en IPv6

        Dim HostName As String

        HostName = Environment.MachineName()

        Dim Hostname2 As System.Net.IPHostEntry = System.Net.Dns.GetHostEntry(HostName)
        Dim ip As System.Net.IPAddress() = Hostname2.AddressList


        Return ip(0).ToString




    End Function

    Public Function toArrayList(ByVal dataTable As DataTable, ByVal collumns As Array) As ArrayList
        ' Recupere l'id de la matricule
        Dim liste As ArrayList = New ArrayList

        For Each row As DataRow In dataTable.Rows

            ' Pour chaque colonne
            For Each collumn As String In collumns

                ' Recuperer la valeur
                Dim value As String = row(collumn)

                ' Enregistrer la valeur
                liste.Add(value)
            Next

        Next


        Return liste
    End Function


    Public Sub afficheDebugArrayList(ByVal dataTable As DataTable, ByVal collumns As Array)
        ' Recupere l'id de la matricule
        Dim liste As ArrayList = New ArrayList

        For Each row As DataRow In dataTable.Rows

            ' Pour chaque colonne
            For Each collumn As String In collumns

                ' Recuperer la valeur
                Dim value As String = row(collumn)

                ' Afficher la valeur
                Debug.WriteLine(value)
            Next

        Next


    End Sub
End Module
