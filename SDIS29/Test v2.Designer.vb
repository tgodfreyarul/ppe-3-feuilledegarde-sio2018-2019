﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class test1
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.button_nbrSemaine = New System.Windows.Forms.Button()
        Me.test2 = New System.Windows.Forms.Button()
        Me.affiche_nbrSemaine = New System.Windows.Forms.Label()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.entree_matricule = New System.Windows.Forms.TextBox()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'button_nbrSemaine
        '
        Me.button_nbrSemaine.Location = New System.Drawing.Point(60, 12)
        Me.button_nbrSemaine.Name = "button_nbrSemaine"
        Me.button_nbrSemaine.Size = New System.Drawing.Size(152, 23)
        Me.button_nbrSemaine.TabIndex = 0
        Me.button_nbrSemaine.Text = "Nombre semaine pompier"
        Me.button_nbrSemaine.UseVisualStyleBackColor = True
        '
        'test2
        '
        Me.test2.Location = New System.Drawing.Point(12, 41)
        Me.test2.Name = "test2"
        Me.test2.Size = New System.Drawing.Size(240, 23)
        Me.test2.TabIndex = 1
        Me.test2.Text = "liste pompiers"
        Me.test2.UseVisualStyleBackColor = True
        '
        'affiche_nbrSemaine
        '
        Me.affiche_nbrSemaine.AutoSize = True
        Me.affiche_nbrSemaine.Location = New System.Drawing.Point(170, 17)
        Me.affiche_nbrSemaine.Name = "affiche_nbrSemaine"
        Me.affiche_nbrSemaine.Size = New System.Drawing.Size(0, 13)
        Me.affiche_nbrSemaine.TabIndex = 2
        '
        'DataGridView1
        '
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(12, 70)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(240, 150)
        Me.DataGridView1.TabIndex = 3
        '
        'entree_matricule
        '
        Me.entree_matricule.Location = New System.Drawing.Point(12, 14)
        Me.entree_matricule.Name = "entree_matricule"
        Me.entree_matricule.Size = New System.Drawing.Size(42, 20)
        Me.entree_matricule.TabIndex = 4
        Me.entree_matricule.Text = "26627"
        '
        'test1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(263, 232)
        Me.Controls.Add(Me.entree_matricule)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.affiche_nbrSemaine)
        Me.Controls.Add(Me.test2)
        Me.Controls.Add(Me.button_nbrSemaine)
        Me.Name = "test1"
        Me.Text = "Test_unitaire"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents button_nbrSemaine As System.Windows.Forms.Button
    Friend WithEvents test2 As System.Windows.Forms.Button
    Friend WithEvents affiche_nbrSemaine As System.Windows.Forms.Label
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents entree_matricule As System.Windows.Forms.TextBox
End Class
