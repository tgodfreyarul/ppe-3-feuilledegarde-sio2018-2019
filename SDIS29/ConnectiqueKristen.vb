﻿Module ConnectiqueKristen
    Public Class Requete


        Public Sub generateEngin()
            Dim casernes As DataTable = Connexion.ORA.Table("Select * from caserne")
            Dim types As DataTable = Connexion.ORA.Table("Select * from type_engin")

            Dim num As Integer = 158

            For Each row As DataRow In casernes.Rows
                Dim random As New Random()
                Dim rndnbr As Integer = random.Next(1, types.Rows.Count)


                For index = 1 To rndnbr
                    Dim type_engin As DataRow = Connexion.ORA.Champ("Select * from type_engin WHERE TYPE_ENG_ID = " & index)
                    Dim immat As String = oui(3, 3)
                    Connexion.ORA.Execute("INSERT INTO Engin (ENGIN_ID, ENGIN_ETAT, ENGIN_NOM, CIS_ID, TYPE_ENG_ID, ENGIN_IMMAT) VALUES(" & num & ", 'OK', '" & type_engin("TYPE_ENG_NOM") & "', " & row("CIS_ID") & ", " & type_engin("TYPE_ENG_ID") & ", '" & immat & "')")

                    num += 1
                Next

            Next
        End Sub


        Public Function oui(ByVal taille, ByVal nb)
            Dim validchars As String = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"

            Dim sb As String = ""
            Dim rand As New Random()

            For index = 1 To nb
                For i As Integer = 1 To taille
                    Dim idx As Integer = rand.Next(0, validchars.Length)
                    Dim randomChar As Char = validchars(idx)
                    sb &= (randomChar)
                Next i

                If (nb <> index) Then
                    sb &= "-"
                End If
            Next


            Return sb
        End Function

    End Class

End Module
