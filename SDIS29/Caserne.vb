﻿Public Class Caserne
    Private _listeMatricule As DataTable
    Private _listeDisponibilitee As ArrayList
    Private _listePompier As ArrayList

    Public Property listeMatricule As DataTable
        Get
            Return Me._listeMatricule
        End Get
        Set(ByVal value As DataTable)
            Me._listeMatricule = value
        End Set
    End Property

    Public Property listeDisponibilitee As ArrayList
        Get
            Return Me._listeDisponibilitee
        End Get
        Set(ByVal value As ArrayList)
            Me._listeDisponibilitee = value
        End Set
    End Property

    Public Property listePompier As ArrayList
        Get
            Return Me._listePompier
        End Get
        Set(ByVal value As ArrayList)
            Me._listePompier = value
        End Set
    End Property


    ' Recupere la liste de tout les pompiers de la meme caserne
    Public Sub fetchListeMatricule(ByVal uneMatricule)
        Dim tableListeMatricule As DataTable = Connexion.ORA.Table("SELECT SP_MATRICULE FROM POMPIER WHERE  SP_MATRICULE <>  26627 AND CIS_ID =" & uneMatricule)
        Debug.WriteLine("debugger")
        afficheDebugArrayList(tableListeMatricule, {"SP_MATRICULE"})

        Me.listeMatricule = tableListeMatricule
    End Sub


    'Recupere la liste d'un pompier
    Public Sub fetchListePompier(ByVal ListeMatricule As DataTable)
        Dim listePompier As ArrayList = New ArrayList

        For Each uneMatricule As DataRow In ListeMatricule.Rows
            Dim matricule As String = uneMatricule("SP_MATRICULE")
            Dim unPompier As utilisateur = New utilisateur

            unPompier.fetchPompier(matricule)

            listePompier.Add(unPompier)
        Next

        Me.listePompier = listePompier

    End Sub

    Public Sub fetchListeDisponibilitee(ByVal ListeMatricule As DataTable)
        Dim listeDispo As ArrayList = New ArrayList

        For Each uneMatricule As DataRow In ListeMatricule.Rows
            Dim matricule As String = uneMatricule("SP_MATRICULE")
            Dim uneDispo As Disponibilitee = New Disponibilitee

            'fetch dispo
            uneDispo.fetchDispo(matricule)

            'Renvoi la liste de dispo
            Dim uneListe As ArrayList = uneDispo.dispo(uneDispo.disponibilitee)
            listeDispo.Add(uneListe)
        Next

        Me.listeDisponibilitee = listeDispo
    End Sub



End Class
