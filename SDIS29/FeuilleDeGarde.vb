﻿Public Class FeuilleDeGarde
    Dim selectionDisponibilitees As Color = Color.FromArgb(255, 128, 128)

    Dim _cnxUtilisateur As utilisateur

    Public Property cnxUtilisateur As utilisateur
        Get
            Return Me._cnxUtilisateur
        End Get
        Set(ByVal value As utilisateur)
            Me._cnxUtilisateur = value
        End Set

    End Property

    Sub New(ByVal unUtilisateur As utilisateur)
        InitializeComponent()
        cnxUtilisateur = unUtilisateur
    End Sub


    'numero jour + numero tranches
    'exemple: 
    '    - lundi ; tranche 2 = 12
    '    - mercredi ; tranche 5 = 35
    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub buttonDisponible_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonDisponible.Click
        selectionDisponibilitees = Color.FromArgb(28, 255, 128)
    End Sub
    Private Sub buttonIndisponible_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonIndisponible.Click
        selectionDisponibilitees = Color.FromArgb(255, 128, 128)
    End Sub

    Private Sub buttonAuTravail_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonAuTravail.Click
        selectionDisponibilitees = Color.FromArgb(128, 255, 255)
    End Sub

    Private Sub Button_Libre_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Libre.Click
        selectionDisponibilitees = Color.White
    End Sub

    Private Sub Button_DeService_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_DeService.Click
        selectionDisponibilitees = Color.Black
    End Sub

    Private Sub button_sendRequest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ' Dans une class <Conversion>
        ' Convertie les couleurs en nombre
        ' Convertie les nombres en couleurs
        ' Soit les nombres, representant les donnees SQL: nombres peut etre remplacés par donnees SQL
        Dim nbrCell As Integer = DataGridView1.Rows(0).Cells.Count
        Dim donneeUtilisateur As New ArrayList
        Dim disponibilite As Integer
        Dim idPlanning As Integer
        Dim tranchePlanning As Integer
        Dim datePlanning As Integer
        Dim idPlanningMax As Integer
        Dim SP_MATRICULE = ecranConnexion.listeUtilisateur.Item(0).matricule
        For indice As Integer = 0 To nbrCell - 1
            Dim listePompier = Connexion.ORA.Table("SELECT ID, TRANCHEID, DATEPLAN FROM PLANNING WHERE SP_MATRICULE =" & SP_MATRICULE & ";")
            For Each row As DataRow In listePompier.Rows
                idPlanning = row("ID")
                tranchePlanning = row("TRANCHEID")
                datePlanning = row("DATEPLAN")
            Next
            If (DataGridView1.Rows(0).Cells(indice).Style.BackColor = Color.FromArgb(28, 255, 128)) Then
                'MsgBox("vert")
                disponibilite = 2
                If (listePompier.Rows.Count() > 0) Then
                    Connexion.ORA.Execute("UPDATE PLANNING SET DISPOID = " & disponibilite & "WHERE SP_MATRICULE = " & SP_MATRICULE & " AND ID = " & idPlanning & ";")
                Else
                    Dim listePompierMax As DataTable = Connexion.ORA.Table("SELECT MAX(ID) FROM PLANNING;")
                    For Each row As DataRow In listePompierMax.Rows
                        idPlanningMax = row("MAX(ID)")
                    Next
                    idPlanningMax += 1
                    Connexion.ORA.Execute("INSERT INTO PLANNING (ID, SP_MATRICULE, TRANCHEID, DISPOID, ETATID, DATEPLAN) VALUES (" & idPlanningMax & ", " & SP_MATRICULE & ", " & tranchePlanning & ", " & disponibilite & ", 0, " & datePlanning & ");")
                End If
            ElseIf (DataGridView1.Rows(0).Cells(indice).Style.BackColor = Color.FromArgb(255, 128, 128)) Then
                'MsgBox("rouge")
                disponibilite = 1
                If (listePompier.Rows.Count() > 0) Then
                    Connexion.ORA.Execute("UPDATE PLANNING SET DISPOID = " & disponibilite & "WHERE SP_MATRICULE = " & SP_MATRICULE & " AND ID = " & idPlanning & ";")
                Else
                    Dim listePompierMax As DataTable = Connexion.ORA.Table("SELECT MAX(ID) FROM PLANNING;")
                    For Each row As DataRow In listePompierMax.Rows
                        idPlanningMax = row("MAX(ID)")
                    Next
                    idPlanningMax += 1
                    Connexion.ORA.Execute("INSERT INTO PLANNING (ID, SP_MATRICULE, TRANCHEID, DISPOID, ETATID, DATEPLAN) VALUES (" & idPlanningMax & ", " & SP_MATRICULE & ", " & tranchePlanning & ", " & disponibilite & ", 0, " & datePlanning & ");")
                End If
            Else
                'MsgBox("bleu")
                disponibilite = 3
                If (listePompier.Rows.Count() > 0) Then
                    Connexion.ORA.Execute("UPDATE PLANNING SET DISPOID = " & disponibilite & "WHERE SP_MATRICULE = " & SP_MATRICULE & " AND ID = " & idPlanning & ";")
                Else
                    Dim listePompierMax As DataTable = Connexion.ORA.Table("SELECT MAX(ID) FROM PLANNING;")
                    For Each row As DataRow In listePompierMax.Rows
                        idPlanningMax = row("MAX(ID)")
                    Next
                    idPlanningMax += 1
                    Connexion.ORA.Execute("INSERT INTO PLANNING (ID, SP_MATRICULE, TRANCHEID, DISPOID, ETATID, DATEPLAN) VALUES (" & idPlanningMax & ", " & SP_MATRICULE & ", " & tranchePlanning & ", " & disponibilite & ", 0, " & datePlanning & ");")
                End If
            End If
            donneeUtilisateur.Add(disponibilite)
            Debug.Print(donneeUtilisateur(indice))
        Next
    End Sub

    Public Sub DataGridView1_CellClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellClick

        'Vérifier si la case est sur la premiere ligne et afficher une msgbox")
        Dim ligneSelected As Integer
        Dim colSelected As Integer
        Dim jourId As Integer
        Dim trancheId As Integer

        ligneSelected = DataGridView1.CurrentCell.RowIndex
        colSelected = DataGridView1.CurrentCell.ColumnIndex

        If (ligneSelected = 0) Then

            ' Recherche de la date sélectionné = lundi de la semaine courante + nb cellule / 4
            jourId = Math.Floor(colSelected / 4)

            Dim unJour As Date = Date.Now
            Dim numeroJour = Weekday(unJour, 0)
            Dim lundiJour As Date = unJour.AddDays(1 - numeroJour)
            Dim jourSelected As String = lundiJour.AddDays(jourId).ToString("d")

            trancheId = (colSelected Mod 4) + 1

            'Clique sur une case
            DataGridView1.CurrentCell.Style.BackColor = selectionDisponibilitees
            DataGridView1.DefaultCellStyle.SelectionBackColor = selectionDisponibilitees

            Dim couleurSelect As Integer
            couleurSelect = getDispo()

            ' Mise à jour de la Table PLANNING lors de l'envoie des modifications
            Dim strSQL As String
            strSQL = "UPDATE PLANNING SET DISPOID=" & couleurSelect & " WHERE PLANNING.SP_MATRICULE= " & Me.cnxUtilisateur.matricule & " AND dateplan = TO_DATE('" & jourSelected & "','dd/MM/yyyy') and trancheId = " & trancheId & ";"
            Connexion.ORA.Execute(strSQL)



        End If

    End Sub

    Private Function getDispo()
        Dim disponibilite As Integer

        If (DataGridView1.CurrentCell.Style.BackColor = Color.FromArgb(28, 255, 128)) Then
            'MsgBox("vert")
            disponibilite = 1
        ElseIf (DataGridView1.CurrentCell.Style.BackColor = Color.FromArgb(255, 128, 128)) Then
            '(DataGridView1.Rows(0).Cells(indice).Style.BackColor = Color.FromArgb(255, 128, 128)) Then
            'MsgBox("rouge")
            disponibilite = 2
        Else
            'MsgBox("bleu")
            disponibilite = 3
        End If
        Return disponibilite
    End Function

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Dim uneHoraire As Date = Date.Now

        Dim uneHeure = uneHoraire.Hour
        Dim uneMinute = uneHoraire.Minute
        Dim uneSeconde = uneHoraire.Second
        Dim lesTranches As New TimeSpan(uneHeure, uneMinute, uneSeconde)
        Dim tableTranche As DataTable = Connexion.ORA.Table("SELECT TRANCHELIB, HOR_DEBUT_SEC, HOR_FIN_SEC FROM TRANCHE;")
        Dim tranchedeb As Integer
        Dim tranchefin As Integer
        Dim tranchelib As String

        For Each row As DataRow In tableTranche.Rows
            tranchedeb = row("HOR_DEBUT_SEC")
            tranchefin = row("HOR_FIN_SEC")
            tranchedeb = tranchedeb / 1000 / 60 / 60
            tranchefin = tranchefin / 1000 / 60 / 60
            If (tranchedeb < Date.Now.Hour < tranchefin) Then
                tranchelib = row("TRANCHELIB")
            End If
        Next

        Select Case lesTranches
            Case New TimeSpan("07", "00", "00") To New TimeSpan("11", "14", "59")
                TextBox1.Text = "Matinée"
                'DataGridView1.ColumnHeadersDefaultCellStyle..DefaultCellStyle.BackColor = Color.Coral
            Case New TimeSpan("11", "15", "00") To New TimeSpan("14", "59", "59")
                TextBox1.Text = "Midi"
            Case New TimeSpan("15", "00", "00") To New TimeSpan("17", "59", "59")
                TextBox1.Text = "Après-midi"
            Case New TimeSpan("18", "00", "00") To New TimeSpan("06", "59", "59")
                TextBox1.Text = "Soirée"
            Case Else
                TextBox1.Text = "Aucune Tranche"
        End Select
        tableTranche.Dispose()
        'TextBox1.Text = lesTranches.ToString
    End Sub




    Private Sub DataGridView2_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles DataGridView2.Scroll
        Dim offset As Integer = DataGridView2.FirstDisplayedScrollingRowIndex.ToString
        DataGridView1.FirstDisplayedScrollingRowIndex = offset
    End Sub

    Private Sub DataGridView1_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles DataGridView1.Scroll
        'Dim offset As Integer = DataGridView1.FirstDisplayedScrollingRowIndex.ToString
        'DataGridView2.FirstDisplayedScrollingRowIndex = offset
    End Sub

    Private Sub GroupBox_disponibilite_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupBox_disponibilite.Enter

    End Sub
End Class
