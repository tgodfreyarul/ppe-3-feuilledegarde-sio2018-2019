﻿Public Class Disponibilitee
    Private _disponibilitee As DataTable

    Public Property disponibilitee As DataTable
        Get
            Return Me._disponibilitee
        End Get
        Set(ByVal value As DataTable)
            Me._disponibilitee = value
        End Set
    End Property

    Public Sub fetchDispo(ByVal uneMatricule)

        Dim unJour As Date = Date.Now
        Dim numeroJour = Weekday(unJour, 0)
        Dim lundi As Date = unJour.AddDays(1 - numeroJour)
        Dim dimanche As Date = lundi.AddDays(6)
        Dim strSQL As String

        Dim lundiJour As String = lundi.ToString("d")
        Dim DimancheJour As String = dimanche.ToString("d")
        '30/04/2019
        strSQL = "SELECT DISPOID FROM PLANNING WHERE SP_MATRICULE =" & uneMatricule & " AND DATEPLAN between TO_DATE('" & lundiJour & "','dd/MM/yyyy') and TO_DATE('" & DimancheJour & "','dd/MM/yyyy') order by DATEPLAN,TRANCHEID;"
        'strSQL = "SELECT DISPOID FROM PLANNING WHERE SP_MATRICULE =" & uneMatricule & " AND DATEPLAN between TO_DATE('27/05/2019','dd/MM/yyyy') and TO_DATE('02/06/2019','dd/MM/yyyy') order by DATEPLAN,TRANCHEID;"
        Dim tableListeDispo As DataTable = Connexion.ORA.Table(strSQL)

        Me.disponibilitee = tableListeDispo

    End Sub

    Public Function dispo(ByVal fetchDispo As DataTable) As ArrayList

        Dim listeDispo As ArrayList = New ArrayList

        For Each uneDispo As DataRow In fetchDispo.Rows
            Dim laDispo As Integer = uneDispo("DISPOID")
            listeDispo.Add(laDispo)
        Next

        Return listeDispo
    End Function













End Class
