﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FeuilleDeGarde

    Inherits System.Windows.Forms.Form


    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.DataGridView2 = New System.Windows.Forms.DataGridView()
        Me.Pompier = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BIP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.buttonAuTravail = New System.Windows.Forms.Button()
        Me.buttonIndisponible = New System.Windows.Forms.Button()
        Me.buttonDisponible = New System.Windows.Forms.Button()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.GroupBox_disponibilite = New System.Windows.Forms.GroupBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupBox_etat = New System.Windows.Forms.GroupBox()
        Me.Label_DeService = New System.Windows.Forms.Label()
        Me.Label_Libre = New System.Windows.Forms.Label()
        Me.Button_DeService = New System.Windows.Forms.Button()
        Me.Button_Libre = New System.Windows.Forms.Button()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox_disponibilite.SuspendLayout()
        Me.GroupBox_etat.SuspendLayout()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AllowUserToResizeColumns = False
        Me.DataGridView1.AllowUserToResizeRows = False
        Me.DataGridView1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DataGridView1.BackgroundColor = System.Drawing.SystemColors.Window
        Me.DataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridView1.DefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridView1.GridColor = System.Drawing.Color.White
        Me.DataGridView1.Location = New System.Drawing.Point(219, 39)
        Me.DataGridView1.MultiSelect = False
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.RowHeadersVisible = False
        Me.DataGridView1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.DataGridView1.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.DataGridView1.Size = New System.Drawing.Size(757, 150)
        Me.DataGridView1.TabIndex = 0
        '
        'DataGridView2
        '
        Me.DataGridView2.AllowUserToAddRows = False
        Me.DataGridView2.AllowUserToDeleteRows = False
        Me.DataGridView2.AllowUserToOrderColumns = True
        Me.DataGridView2.AllowUserToResizeRows = False
        Me.DataGridView2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DataGridView2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DataGridView2.BackgroundColor = System.Drawing.Color.White
        Me.DataGridView2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.DataGridView2.ColumnHeadersHeight = 46
        Me.DataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.DataGridView2.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Pompier, Me.BIP})
        Me.DataGridView2.GridColor = System.Drawing.Color.SeaShell
        Me.DataGridView2.Location = New System.Drawing.Point(12, 39)
        Me.DataGridView2.Name = "DataGridView2"
        Me.DataGridView2.ReadOnly = True
        Me.DataGridView2.RowHeadersVisible = False
        Me.DataGridView2.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.DataGridView2.Size = New System.Drawing.Size(201, 150)
        Me.DataGridView2.TabIndex = 2
        '
        'Pompier
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.White
        Me.Pompier.DefaultCellStyle = DataGridViewCellStyle2
        Me.Pompier.FillWeight = 159.3909!
        Me.Pompier.HeaderText = "Pompier"
        Me.Pompier.Name = "Pompier"
        Me.Pompier.ReadOnly = True
        '
        'BIP
        '
        Me.BIP.FillWeight = 40.60914!
        Me.BIP.HeaderText = "BIP"
        Me.BIP.Name = "BIP"
        Me.BIP.ReadOnly = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 13)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(85, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Tranche horaire:"
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.SystemColors.Info
        Me.TextBox1.Enabled = False
        Me.TextBox1.Location = New System.Drawing.Point(103, 10)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(110, 20)
        Me.TextBox1.TabIndex = 4
        '
        'buttonAuTravail
        '
        Me.buttonAuTravail.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.buttonAuTravail.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.buttonAuTravail.Location = New System.Drawing.Point(311, 37)
        Me.buttonAuTravail.Name = "buttonAuTravail"
        Me.buttonAuTravail.Size = New System.Drawing.Size(18, 18)
        Me.buttonAuTravail.TabIndex = 22
        Me.buttonAuTravail.UseVisualStyleBackColor = False
        '
        'buttonIndisponible
        '
        Me.buttonIndisponible.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.buttonIndisponible.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.buttonIndisponible.Location = New System.Drawing.Point(220, 37)
        Me.buttonIndisponible.Name = "buttonIndisponible"
        Me.buttonIndisponible.Size = New System.Drawing.Size(18, 18)
        Me.buttonIndisponible.TabIndex = 21
        Me.buttonIndisponible.UseVisualStyleBackColor = False
        '
        'buttonDisponible
        '
        Me.buttonDisponible.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.buttonDisponible.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.buttonDisponible.Location = New System.Drawing.Point(112, 37)
        Me.buttonDisponible.Name = "buttonDisponible"
        Me.buttonDisponible.Size = New System.Drawing.Size(18, 18)
        Me.buttonDisponible.TabIndex = 20
        Me.buttonDisponible.UseVisualStyleBackColor = False
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        '
        'GroupBox_disponibilite
        '
        Me.GroupBox_disponibilite.Controls.Add(Me.Label4)
        Me.GroupBox_disponibilite.Controls.Add(Me.Label3)
        Me.GroupBox_disponibilite.Controls.Add(Me.Label2)
        Me.GroupBox_disponibilite.Controls.Add(Me.buttonDisponible)
        Me.GroupBox_disponibilite.Controls.Add(Me.buttonIndisponible)
        Me.GroupBox_disponibilite.Controls.Add(Me.buttonAuTravail)
        Me.GroupBox_disponibilite.Location = New System.Drawing.Point(636, 195)
        Me.GroupBox_disponibilite.Name = "GroupBox_disponibilite"
        Me.GroupBox_disponibilite.Size = New System.Drawing.Size(340, 59)
        Me.GroupBox_disponibilite.TabIndex = 24
        Me.GroupBox_disponibilite.TabStop = False
        Me.GroupBox_disponibilite.Text = "DISPONIBILITE"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(244, 40)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(61, 13)
        Me.Label4.TabIndex = 26
        Me.Label4.Text = "Au Travail :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(145, 40)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(69, 13)
        Me.Label3.TabIndex = 30
        Me.Label3.Text = "Indisponbile :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(44, 40)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(62, 13)
        Me.Label2.TabIndex = 29
        Me.Label2.Text = "Disponible :"
        '
        'GroupBox_etat
        '
        Me.GroupBox_etat.Controls.Add(Me.Label_DeService)
        Me.GroupBox_etat.Controls.Add(Me.Label_Libre)
        Me.GroupBox_etat.Controls.Add(Me.Button_DeService)
        Me.GroupBox_etat.Controls.Add(Me.Button_Libre)
        Me.GroupBox_etat.Location = New System.Drawing.Point(15, 195)
        Me.GroupBox_etat.Name = "GroupBox_etat"
        Me.GroupBox_etat.Size = New System.Drawing.Size(198, 59)
        Me.GroupBox_etat.TabIndex = 25
        Me.GroupBox_etat.TabStop = False
        Me.GroupBox_etat.Text = "ETAT"
        '
        'Label_DeService
        '
        Me.Label_DeService.AutoSize = True
        Me.Label_DeService.Location = New System.Drawing.Point(99, 26)
        Me.Label_DeService.Name = "Label_DeService"
        Me.Label_DeService.Size = New System.Drawing.Size(66, 13)
        Me.Label_DeService.TabIndex = 24
        Me.Label_DeService.Text = "De Service :"
        '
        'Label_Libre
        '
        Me.Label_Libre.AutoSize = True
        Me.Label_Libre.Location = New System.Drawing.Point(22, 26)
        Me.Label_Libre.Name = "Label_Libre"
        Me.Label_Libre.Size = New System.Drawing.Size(36, 13)
        Me.Label_Libre.TabIndex = 23
        Me.Label_Libre.Text = "Libre :"
        '
        'Button_DeService
        '
        Me.Button_DeService.BackColor = System.Drawing.Color.Black
        Me.Button_DeService.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button_DeService.Location = New System.Drawing.Point(171, 21)
        Me.Button_DeService.Name = "Button_DeService"
        Me.Button_DeService.Size = New System.Drawing.Size(18, 18)
        Me.Button_DeService.TabIndex = 22
        Me.Button_DeService.UseVisualStyleBackColor = False
        '
        'Button_Libre
        '
        Me.Button_Libre.BackColor = System.Drawing.Color.White
        Me.Button_Libre.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button_Libre.Location = New System.Drawing.Point(64, 19)
        Me.Button_Libre.Name = "Button_Libre"
        Me.Button_Libre.Size = New System.Drawing.Size(18, 18)
        Me.Button_Libre.TabIndex = 21
        Me.Button_Libre.UseVisualStyleBackColor = False
        '
        'FeuilleDeGarde
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.ClientSize = New System.Drawing.Size(988, 290)
        Me.Controls.Add(Me.GroupBox_etat)
        Me.Controls.Add(Me.GroupBox_disponibilite)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.DataGridView2)
        Me.Controls.Add(Me.DataGridView1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "FeuilleDeGarde"
        Me.Text = "Form1"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox_disponibilite.ResumeLayout(False)
        Me.GroupBox_disponibilite.PerformLayout()
        Me.GroupBox_etat.ResumeLayout(False)
        Me.GroupBox_etat.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    'Dim days As String() = {"Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"}

    Dim days As New ArrayList

    Private Sub Combine_two_columns_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim unUtilisateur As utilisateur = Me._cnxUtilisateur
        Dim dataCollums As New DataGridViewColumn
        Dim collectionUtilisateur As New List(Of Object)
        dataCollums.CellTemplate = New DataGridViewTextBoxCell 'I had to add this to my code

        Dim jourFormat As New ArrayList
        Dim unJour As Date = Date.Now
        Dim unFormat As Date = FormatDateTime(unJour, DateFormat.LongDate)

        Dim numeroJour = Weekday(unJour, 0)
        Dim lundiJour As Date = unJour.AddDays(1 - numeroJour)

        For i As Integer = 1 To 7 Step 1
            jourFormat.Add(lundiJour)
            lundiJour = lundiJour.ToString("ddd d MMM")
            days.Add(lundiJour)
            lundiJour = lundiJour.AddDays(1)
        Next

        For Each lst As Object In days
            ' CREATE Tranche
            For i = 0 To 3
                Me.DataGridView1.Columns.Add(i + 1, i + 1)
            Next

            'ADD DATA
            'Me.DataGridView1.Rows.Add(reduct, "", "0", "2")
        Next



        'Recuperer tout les infos d'un pompier
        'unUtilisateur.fetchPompier("26956")
        '=========================
        ' GRADE PAS BON ????
        '=========================



        'Recuperer les imatricules de la même caserne de ce pompier
        Dim uneCaserne As Caserne = New Caserne
        uneCaserne.fetchListeMatricule(unUtilisateur.caserneID)



        'Pour chaque imatricules récupérer la liste de chaque pompier
        uneCaserne.fetchListePompier(uneCaserne.listeMatricule)



        'Pour chaque imatricules récupérer la liste de chaque disponibilitee d'un pompier
        uneCaserne.fetchListeDisponibilitee(uneCaserne.listeMatricule)


        'Afficher la liste de tous les pompiers (colonne gauche)
        ' Affiche le pompier  connecté à la ligne 0
        Dim lePompier As String = unUtilisateur.pompier(unUtilisateur)
        Me.DataGridView2.Rows.Add({lePompier, unUtilisateur.bip})
        ' Affiche le reste des pompier
        affichePompier(uneCaserne.listePompier, 1)


        'Afficher la liste des dispo (colonne droite)

        'Afficher la liste de tout les disponibilitee du pompier connecté
        Dim uneDispo As Disponibilitee = New Disponibilitee

        'Recupere les disponibilitees dans la table
        uneDispo.fetchDispo(unUtilisateur.matricule)

        Dim planingDispo As ArrayList = uneDispo.dispo(uneDispo.disponibilitee)
        afficheDispo(planingDispo)

        'Afficher la liste de toutes les disponibilitee de tous les autres

        'Debug.WriteLine(uneCaserne.listeDisponibilitee.Item(0))
        Dim ligneLaDispo = 1
        For Each laDispo As ArrayList In uneCaserne.listeDisponibilitee
            ligneLaDispo = afficheDispo(laDispo, ligneLaDispo)
        Next








        'En admin possiblité de voir GroupBox_etat + GroupBox_Disponibilite



        For j As Integer = 0 To Me.DataGridView1.ColumnCount - 1

        Next
        Me.DataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing
        Me.DataGridView1.ColumnHeadersHeight = Me.DataGridView1.ColumnHeadersHeight * 2

        Me.DataGridView1.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter

        ''ajout autorisations de visualisation
        If unUtilisateur.grade = 0 Then
            GroupBox_etat.Visible = False
        Else
            GroupBox_etat.Visible = True
            GroupBox_disponibilite.Visible = True
        End If
    End Sub

    Public Function afficheDispo(ByVal planingDispo, Optional ByVal laLigne = 0, Optional ByVal laColonne = 0) As Integer



        For Each uneDisponibilite As Object In planingDispo 'POUR CHAQUE NUMERO
            Me.DataGridView1.Rows.Add() 'INSERER DATA

            'Me.DataGridView1.Rows(donneeLigne).Cells(laColonne).Value = uneDisponibilite 'Ecris un nombre dans le DataGrid

            If (uneDisponibilite = 1) Then 'SI Cellule = 1: Couleur vert
                DataGridView1.Rows(laLigne).Cells(laColonne).Style.BackColor = Color.FromArgb(28, 255, 128)
            ElseIf (uneDisponibilite = 2) Then 'SI Cellule = 2: Couleur rouge
                DataGridView1.Rows(laLigne).Cells(laColonne).Style.BackColor = Color.FromArgb(255, 128, 128)
            ElseIf (uneDisponibilite = 3) Then 'SI Cellule = 3: Couleur bleu
                DataGridView1.Rows(laLigne).Cells(laColonne).Style.BackColor = Color.FromArgb(128, 255, 255)
            End If
            laColonne += 1



        Next
        laLigne += 1
        laColonne += 0
        Return laLigne
    End Function


    Public Function affichePompier(ByVal pompiers, Optional ByVal laLigne = 0) As Integer
        For Each unPompier As Object In pompiers
            Dim lePompier As String = Me._cnxUtilisateur.pompier(unPompier)
            Me.DataGridView2.Rows.Add({lePompier, unPompier.bip})
        Next

        laLigne += 1

        Return laLigne
    End Function

    Private Sub DataGridView1_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles DataGridView1.Paint


        ' DRAWING
        Try
            Me.DataGridView1.CurrentCell.Selected = False
        Catch ex As Exception

        End Try



        Dim j As Integer = 0
        Dim jCount As Integer = 0
        ' Nombre de collone
        Dim nbrCollumn = 4
        While j < Me.DataGridView1.ColumnCount
            jCount += 1
            Debug.WriteLine(jCount)

            Dim r1 As Rectangle = Me.DataGridView1.GetCellDisplayRectangle(j, -1, True)
            Dim w2 As Integer = Me.DataGridView1.GetCellDisplayRectangle(j + nbrCollumn - 1, -1, True).Width
            Dim days_date As Date = days(j \ nbrCollumn)
            Dim days_size As String = days_date.ToString("ddd d MMM")


            r1.X += 1
            r1.Y += 1

            r1.Width = r1.Width * nbrCollumn - 2
            r1.Height = r1.Height / 2 - 2
            e.Graphics.FillRectangle(New SolidBrush(Me.DataGridView1.ColumnHeadersDefaultCellStyle.BackColor), r1)
            Dim format As New StringFormat()
            format.Alignment = StringAlignment.Center
            format.LineAlignment = StringAlignment.Center

            'days(j \ nbrCollumn)
            e.Graphics.DrawString(days_size, Me.DataGridView1.ColumnHeadersDefaultCellStyle.Font, New SolidBrush(Me.DataGridView1.ColumnHeadersDefaultCellStyle.ForeColor), r1, format)
            j += nbrCollumn
        End While
    End Sub

    'https://social.msdn.microsoft.com/Forums/en-US/4a3f5639-8c2b-4a86-ae08-28abb9c8f845/how-to-merge-two-column-headers-in-a-datagridview?forum=vbgeneral

    'Private Sub DataGridView1_CellPainting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles DataGridView1.CellPainting
    '    If e.RowIndex = -1 AndAlso e.ColumnIndex > -1 Then
    '        Dim r2 As Rectangle = e.CellBounds
    '        r2.Y += e.CellBounds.Height / 2
    '        r2.Height = e.CellBounds.Height / 2
    '        e.PaintBackground(r2, True)
    '        e.PaintContent(r2)
    '        e.Handled = True
    '    End If
    'End Sub
    'Private Sub DataGridView1_ColumnWidthChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewColumnEventArgs) Handles DataGridView1.ColumnWidthChanged
    '    Dim rtHeader As Rectangle = Me.DataGridView1.DisplayRectangle
    '    rtHeader.Height = Me.DataGridView1.ColumnHeadersHeight / 2
    '    Me.DataGridView1.Invalidate(rtHeader)
    'End Sub

    'Private Sub DataGridView1_Scroll(ByVal sender As Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles DataGridView1.Scroll
    '    Dim rtHeader As Rectangle = Me.DataGridView1.DisplayRectangle
    '    rtHeader.Height = Me.DataGridView1.ColumnHeadersHeight / 2
    '    Me.DataGridView1.Invalidate(rtHeader)
    'End Sub

    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridView2 As System.Windows.Forms.DataGridView
    Friend WithEvents Pompier As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BIP As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents buttonAuTravail As System.Windows.Forms.Button
    Friend WithEvents buttonIndisponible As System.Windows.Forms.Button
    Friend WithEvents buttonDisponible As System.Windows.Forms.Button
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    'Friend WithEvents Pompier As System.Windows.Forms.DataGridViewTextBoxColumn
    'Friend WithEvents BIP As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GroupBox_disponibilite As System.Windows.Forms.GroupBox
    Friend WithEvents Button_Libre As System.Windows.Forms.Button
    Friend WithEvents Button_DeService As System.Windows.Forms.Button
    Friend WithEvents Label_Libre As System.Windows.Forms.Label
    Friend WithEvents Label_DeService As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents GroupBox_etat As System.Windows.Forms.GroupBox


End Class

